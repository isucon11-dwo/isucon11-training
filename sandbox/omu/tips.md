
# メモ

## リモートのソースコードをローカルにコピー

```bash
rsync -av -e ssh --exclude='target' is1:/home/isucon/isuumo/webapp/rust .
```

## ローカルで MySQL を動かす

```bash
docker-compose up -d

MYSQL_HOST=$(hostname -I | awk '{print $1}')

docker run --rm \
  -v $(pwd)/config/~/isuumo/webapp/mysql/db:/db \
  -e MYSQL_HOST=${MYSQL_HOST} mysql:8 bash -c "cp -r /db /bd; chmod +x /bd/init.sh && /bd/init.sh"

docker run -it --rm mysql:8 mysql --host=${MYSQL_HOST} --password=root --user=root
```

## Nginx 設定のバリデーション

```bash
docker run -it -v $(pwd)/config:/config --entrypoint="" nginx \
    bash -c "cp /config/etc/hosts /etc/hosts; nginx -t -c /config/etc/nginx/nginx.conf"
```

## MySql 設定のバリデーション

```bash
docker run -it -v $(pwd)/config:/config mysql:8 \
    bash -c "mkdir /var/lib/mysql-files; rm -rf /etc/mysql; cp -r /config/etc/mysql /etc/mysql; mysqld --validate-config"
```