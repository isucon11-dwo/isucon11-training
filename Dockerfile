FROM rust:1.54.0-buster AS builder

WORKDIR /app

RUN cargo search tokio

COPY Cargo.* .
COPY src src

RUN --mount=type=cache,target=/usr/local/cargo/registry \
    --mount=type=cache,target=/app/target \
    cargo install --path .

FROM scratch

COPY --from=builder /usr/local/cargo/bin/isuumo /
