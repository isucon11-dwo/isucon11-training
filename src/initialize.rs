use actix_web::error::InternalError;
use actix_web::http::StatusCode;
use actix_web::{web, Error as AWError, HttpResponse};
use serde::Serialize;
use std::env;

#[derive(Debug)]
pub struct MySQLConnectionEnv {
    pub host: String,
    pub port: u16,
    pub user: String,
    pub db_name: String,
    pub password: String,
}

impl Default for MySQLConnectionEnv {
    fn default() -> Self {
        let port = if let Ok(port) = env::var("MYSQL_PORT") {
            port.parse().unwrap_or(3306)
        } else {
            3306
        };
        Self {
            host: env::var("MYSQL_HOST").unwrap_or_else(|_| "127.0.0.1".to_owned()),
            port,
            user: env::var("MYSQL_USER").unwrap_or_else(|_| "isucon".to_owned()),
            db_name: env::var("MYSQL_DBNAME").unwrap_or_else(|_| "isuumo".to_owned()),
            password: env::var("MYSQL_PASS").unwrap_or_else(|_| "isucon".to_owned()),
        }
    }
}

#[derive(Debug, Serialize)]
struct InitializeResponse {
    language: String,
}

pub async fn initialize(
    mysql_connection_env: web::Data<MySQLConnectionEnv>,
) -> Result<HttpResponse, AWError> {
    let sql_dir = std::path::Path::new("..").join("mysql").join("db");
    let paths = [
        sql_dir.join("0_Schema.sql"),
        sql_dir.join("1_DummyEstateData.sql"),
        sql_dir.join("2_DummyChairData.sql"),
        sql_dir.join("3_alter.sql"),
        sql_dir.join("4_geometry.sql"),
    ];
    for p in paths.iter() {
        let sql_file = p.canonicalize().unwrap();
        let cmd_str = format!(
            "mysql -h {} -P {} -u {} -p{} {} < {}",
            mysql_connection_env.host,
            mysql_connection_env.port,
            mysql_connection_env.user,
            mysql_connection_env.password,
            mysql_connection_env.db_name,
            sql_file.display()
        );
        let status = tokio::process::Command::new("bash")
            .arg("-c")
            .arg(cmd_str)
            .status()
            .await
            .map_err(|e| {
                log::error!("Initialize script {} failed : {:?}", p.display(), e);
                InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
            })?;
        if !status.success() {
            log::error!("Initialize script {} failed", p.display());
            return Ok(HttpResponse::InternalServerError().finish());
        }
    }
    Ok(HttpResponse::Ok().json(InitializeResponse {
        language: "rust".to_owned(),
    }))
}
