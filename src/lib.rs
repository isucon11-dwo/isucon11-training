pub mod chair;
pub mod common;
pub mod estate;
pub mod initialize;
pub mod map;

pub type Pool = r2d2::Pool<r2d2_mysql::MysqlConnectionManager>;
pub type BlockingDBError = actix_web::error::BlockingError;

pub const LIMIT: i64 = 20;
