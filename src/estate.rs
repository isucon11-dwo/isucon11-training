use actix_multipart::Multipart;
use actix_web::{error::InternalError, http::StatusCode, web, Error as AWError, HttpResponse};
use bytes::BytesMut;
use futures::TryStreamExt;
use mysql::prelude::*;
use serde::{Deserialize, Serialize};
use tokio::sync::RwLock;

use crate::{chair::Chair, common::*, map::LockedHashMap, BlockingDBError, Pool, LIMIT};

const NAZOTTE_LIMIT: usize = 50;

pub type EstateSearchCache = LockedHashMap<String, EstateSearchResponse>;
pub type EstateDetailCache = LockedHashMap<i64, Estate>;
pub type EstateLowPricedCache = RwLock<Option<EstateListResponse>>;

#[derive(Debug, Deserialize, Serialize)]
pub struct EstateSearchCondition {
    #[serde(rename = "doorWidth")]
    door_width: RangeCondition,
    #[serde(rename = "doorHeight")]
    door_height: RangeCondition,
    rent: RangeCondition,
    feature: ListCondition,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Estate {
    id: i64,
    name: String,
    description: String,
    thumbnail: String,
    address: String,
    latitude: f64,
    longitude: f64,
    rent: i64,
    #[serde(rename = "doorHeight")]
    door_height: i64,
    #[serde(rename = "doorWidth")]
    door_width: i64,
    features: String,
}

impl FromRow for Estate {
    fn from_row_opt(row: mysql::Row) -> Result<Self, mysql::FromRowError> {
        fn convert(row: &mysql::Row) -> Result<Estate, ()> {
            Ok(Estate {
                id: row.get("id").ok_or(())?,
                thumbnail: row.get("thumbnail").ok_or(())?,
                name: row.get("name").ok_or(())?,
                description: row.get("description").ok_or(())?,
                latitude: row.get("latitude").ok_or(())?,
                longitude: row.get("longitude").ok_or(())?,
                address: row.get("address").ok_or(())?,
                rent: row.get("rent").ok_or(())?,
                door_height: row.get("door_height").ok_or(())?,
                door_width: row.get("door_width").ok_or(())?,
                features: row.get("features").ok_or(())?,
            })
        }
        convert(&row).map_err(|_| mysql::FromRowError(row))
    }
}

pub async fn get_estate_detail(
    db: web::Data<Pool>,
    cache: web::Data<EstateDetailCache>,
    path: web::Path<i64>,
) -> Result<HttpResponse, AWError> {
    let id = path.into_inner();
    if let Some(estate) = cache.read(&id).await.get(&id) {
        return Ok(HttpResponse::Ok().json(estate));
    }

    let estate: Option<Estate> = web::block(move || {
        let mut conn = db.get().expect("Failed to checkout database connection");
        conn.exec_first("select id,thumbnail,name,description,latitude,longitude,address,rent,door_height,door_width,features from estate where id = ?", (id,))
    })
    .await
    .map_err(|e| {
        log::error!("Database Execution error : {:?}", e);
        e
    })?
    .map_err(|e| {
        log::error!("Database Execution error : {:?}", e);
        InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
    })?;

    if let Some(estate) = estate {
        cache.write(&id).await.insert(id, estate.clone());
        Ok(HttpResponse::Ok().json(estate))
    } else {
        Ok(HttpResponse::NotFound().finish())
    }
}

#[derive(Debug, Deserialize)]
pub struct CSVEstate {
    id: i64,
    name: String,
    description: String,
    thumbnail: String,
    address: String,
    latitude: f64,
    longitude: f64,
    rent: i64,
    door_height: i64,
    door_width: i64,
    features: String,
    popularity: i64,
}

impl Into<Estate> for CSVEstate {
    fn into(self) -> Estate {
        Estate {
            id: self.id,
            name: self.name,
            description: self.description,
            thumbnail: self.thumbnail,
            address: self.address,
            latitude: self.latitude,
            longitude: self.longitude,
            rent: self.rent,
            door_height: self.door_height,
            door_width: self.door_width,
            features: self.features,
        }
    }
}

pub async fn post_estate(
    db: web::Data<Pool>,
    cache: web::Data<EstateSearchCache>,
    cache2: web::Data<EstateLowPricedCache>,
    mut payload: Multipart,
) -> Result<HttpResponse, AWError> {
    let mut estates: Option<Vec<CSVEstate>> = None;
    while let Ok(Some(field)) = payload.try_next().await {
        let content_disposition = field.content_disposition().unwrap();
        let name = content_disposition.get_name().unwrap();
        if name == "estates" {
            let content = field
                .map_ok(|chunk| BytesMut::from(&chunk[..]))
                .try_concat()
                .await?;
            let mut reader = csv::ReaderBuilder::new()
                .has_headers(false)
                .from_reader(content.as_ref());
            let mut es = Vec::new();
            for record in reader.deserialize() {
                let estate: CSVEstate = record.map_err(|e| {
                    log::error!("failed to read csv: {:?}", e);
                    InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
                })?;
                es.push(estate);
            }
            estates = Some(es);
        }
    }
    if estates.is_none() {
        log::error!("failed to get from file: no estates given");
        return Ok(HttpResponse::BadRequest().finish());
    }
    let estates = estates.unwrap();

    web::block(move || -> Result<(), mysql::Error> {
        let mut conn = db.get().expect("Failed to checkout database connection");
        let mut stmt = String::from("insert into estate (id, name, description, thumbnail, address, latitude, longitude, rent, door_height, door_width, features, popularity, geo) values");
        let mut params = Vec::<mysql::Value>::new();
        for (i, estate) in estates.into_iter().enumerate() {
            let geom = format!("ST_GeomFromText('Point({} {})', 4326)", estate.latitude, estate.longitude);
            if i != 0 {
                stmt.push(',');
            }
            stmt.push_str(&format!("(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, {})", geom));
            params.push(estate.id.into());
            params.push(estate.name.into());
            params.push(estate.description.into());
            params.push(estate.thumbnail.into());
            params.push(estate.address.into());
            params.push(estate.latitude.into());
            params.push(estate.longitude.into());
            params.push(estate.rent.into());
            params.push(estate.door_height.into());
            params.push(estate.door_width.into());
            params.push(estate.features.into());
            params.push(estate.popularity.into());
        }
        let mut tx = conn.start_transaction(mysql::TxOpts::default())?;
        tx.exec_drop(stmt, params)?;
        tx.commit()?;
        Ok(())
    }).await.map_err(
        |e: BlockingDBError| {
            log::error!("failed to insert/commit estate: {:?}", e);
            e
        },
    )?.map_err(|e| {
        log::error!("failed to read csv: {:?}", e);
        InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
    })?;
    cache.clear().await;
    cache2.write().await.take();
    Ok(HttpResponse::Created().finish())
}

#[derive(Debug, Deserialize)]
pub struct SearchEstatesParams {
    #[serde(rename = "doorHeightRangeId", default)]
    door_height_range_id: String,
    #[serde(rename = "doorWidthRangeId", default)]
    door_width_range_id: String,
    #[serde(rename = "rentRangeId", default)]
    rent_range_id: String,
    #[serde(default)]
    features: String,
    page: i64,
    #[serde(rename = "perPage")]
    per_page: i64,
}

#[derive(Debug, Serialize, Clone)]
pub struct EstateSearchResponse {
    count: i64,
    estates: Vec<Estate>,
}

pub async fn search_estates(
    db: web::Data<Pool>,
    cache: web::Data<EstateSearchCache>,
    query_params: web::Query<SearchEstatesParams>,
) -> Result<HttpResponse, AWError> {
    let mut conditions = Vec::new();
    let mut params: Vec<mysql::Value> = Vec::new();

    if !query_params.door_height_range_id.is_empty() {
        match query_params.door_height_range_id.parse::<i8>() {
            Ok(i) if 0 <= i && i <= 3 => {
                conditions.push("door_height_id = ?");
                params.push(i.into());
            }
            _ => return Ok(HttpResponse::BadRequest().finish()),
        }
    }

    if !query_params.door_width_range_id.is_empty() {
        match query_params.door_width_range_id.parse::<i8>() {
            Ok(i) if 0 <= i && i <= 3 => {
                conditions.push("door_width_id = ?");
                params.push(i.into());
            }
            _ => return Ok(HttpResponse::BadRequest().finish()),
        }
    }

    if !query_params.rent_range_id.is_empty() {
        match query_params.rent_range_id.parse::<i8>() {
            Ok(i) if 0 <= i && i <= 3 => {
                conditions.push("rent_id = ?");
                params.push(i.into());
            }
            _ => return Ok(HttpResponse::BadRequest().finish()),
        }
    }

    if !query_params.features.is_empty() {
        for f in query_params.features.split(',') {
            conditions.push("features like concat('%', ?, '%')");
            params.push(f.into());
        }
    }

    if conditions.is_empty() {
        log::info!("search_estates search condition not found");
        return Ok(HttpResponse::BadRequest().finish());
    }

    let per_page = query_params.per_page;
    let page = query_params.page;

    let search_condition = conditions.join(" and ");
    if let Some(v) = cache.read(&search_condition).await.get(&search_condition) {
        return Ok(HttpResponse::Ok().json(v));
    }
    let key = search_condition.clone();
    let res = web::block(move || -> Result<EstateSearchResponse, mysql::Error> {
        let mut conn = db.get().expect("Failed to checkout database connection");
        let row = conn.exec_first(
            format!("select count(*) from estate where {}", search_condition),
            &params,
        )?;
        let count = row.map(|(c,)| c).unwrap_or(0);
        if count == 0 {
            return Ok(EstateSearchResponse {count, estates: Vec::new()});
        }
        params.push(per_page.into());
        params.push((page * per_page).into());
        let estates = conn.exec(
            format!(
                "select id,thumbnail,name,description,latitude,longitude,address,rent,door_height,door_width,features from estate where {} order by popularity desc, id asc limit ? offset ?",
                search_condition
            ),
            &params,
        )?;
        Ok(EstateSearchResponse { count, estates })
    })
    .await
    .map_err(|e: BlockingDBError| {
        log::error!("search_estates DB execution error : {:?}", e);
        e
    })?
    .map_err(|e| {
        log::error!("search_estates DB execution error : {:?}", e);
        InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
    })?;
    cache.write(&key).await.insert(key, res.clone());
    Ok(HttpResponse::Ok().json(res))
}

#[derive(Debug, Serialize, Clone)]
pub struct EstateListResponse {
    estates: Vec<Estate>,
}

pub async fn get_low_priced_estate(
    db: web::Data<Pool>,
    cache: web::Data<EstateLowPricedCache>,
) -> Result<HttpResponse, AWError> {
    if let Some(r) = cache.read().await.as_ref() {
        return Ok(HttpResponse::Ok().json(r));
    }
    let estates = web::block(move || -> Result<Vec<Estate>, mysql::Error> {
        let mut conn = db.get().expect("Failed to checkout database connection");
        conn.exec(
            "select id,thumbnail,name,description,latitude,longitude,address,rent,door_height,door_width,features from estate order by rent asc, id asc limit ?",
            (LIMIT,),
        )
    })
    .await
    .map_err(|e| {
        log::error!("get_low_priced_estate DB execution error : {:?}", e);
        e
    })?
    .map_err(|e| {
        log::error!("get_low_priced_estate DB execution error : {:?}", e);
        InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
    })?;

    let resp = EstateListResponse { estates };
    let r = Ok(HttpResponse::Ok().json(&resp));
    cache.write().await.replace(resp);
    r
}

pub async fn get_estate_search_condition(
    estate_search_condition: web::Data<EstateSearchCondition>,
) -> Result<HttpResponse, AWError> {
    Ok(HttpResponse::Ok().json(estate_search_condition.as_ref()))
}

pub async fn search_recommended_estate_with_chair(
    db: web::Data<Pool>,
    path: web::Path<i64>,
) -> Result<HttpResponse, AWError> {
    let id = path.into_inner();

    let estates = web::block(move || -> Result<Option<Vec<Estate>>, mysql::Error> {
        let mut conn = db.get().expect("Failed to checkout database connection");
        let chair: Option<Chair> = conn.exec_first("select id,name,description,thumbnail,price,height,width,depth,color,features,kind,stock from chair where id = ?", (id,))?;
        if let Some(chair) = chair {
            let mut v = vec![chair.width, chair.height, chair.depth];
            v.sort();
            let query = r"select id,thumbnail,name,description,latitude,longitude,address,rent,door_height,door_width,features from estate
            where door_min >= ? and door_max >= ?
            order by popularity desc, id asc limit ?";
            Ok(Some(conn.exec(query, (v[0], v[1], LIMIT))?))
        } else {
            Ok(None)
        }
    })
    .await
    .map_err(|e: BlockingDBError| {
        log::error!("Database execution error : {:?}", e);
        e
    })?
    .map_err(|e| {
        log::error!("Database execution error : {:?}", e);
        InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
    })?;

    if let Some(estates) = estates {
        Ok(HttpResponse::Ok().json(EstateListResponse { estates }))
    } else {
        log::info!("Requested chair id \"{}\" not found", id);
        Ok(HttpResponse::BadRequest().finish())
    }
}

#[derive(Debug, Deserialize)]
pub struct Coordinates {
    coordinates: Vec<Coordinate>,
}

#[derive(Debug, Deserialize)]
pub struct Coordinate {
    latitude: f64,
    longitude: f64,
}

impl Coordinates {
    fn coordinates_to_text(&self) -> String {
        let points: Vec<_> = self
            .coordinates
            .iter()
            .map(|c| format!("{} {}", c.latitude, c.longitude))
            .collect();
        format!("'POLYGON(({}))'", points.join(","))
    }
}

#[derive(Debug)]
pub struct BoundingBox {
    top_left_corner: Coordinate,
    bottom_right_corner: Coordinate,
}

pub async fn search_estate_nazotte(
    db: web::Data<Pool>,
    coordinates: web::Json<Coordinates>,
) -> Result<HttpResponse, AWError> {
    if coordinates.coordinates.is_empty() {
        return Ok(HttpResponse::BadRequest().finish());
    }

    let estates = web::block(move || -> Result<Vec<Estate>, mysql::Error> {
        let mut conn = db.get().expect("Failed to checkout database connection");
        let query = format!("select id,thumbnail,name,description,latitude,longitude,address,rent,door_height,door_width,features from estate where ST_Contains(ST_PolygonFromText({}, 4326), geo) order by popularity desc, id asc LIMIT ?", coordinates.coordinates_to_text());
        let estates_in_polygon: Vec<Estate> = conn.exec(query, (NAZOTTE_LIMIT, ))?;
        if estates_in_polygon.is_empty() {
            return Ok(Vec::new());
        }
        Ok(estates_in_polygon)
    })
        .await
        .map_err(|e: BlockingDBError| {
            log::error!("Database execution error : {:?}", e);
            e
        })?.map_err(|e| {
        log::error!("Database execution error : {:?}", e);
        InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
    })?;

    Ok(HttpResponse::Ok().json(EstateSearchResponse {
        count: estates.len() as i64,
        estates,
    }))
}

#[derive(Debug, Deserialize)]
pub struct PostEstateRequestDocumentParams {
    email: String,
}

pub async fn post_estate_request_document(
    db: web::Data<Pool>,
    path: web::Path<i64>,
    cache: web::Data<EstateDetailCache>,
    _params: web::Json<PostEstateRequestDocumentParams>,
) -> Result<HttpResponse, AWError> {
    let id = path.into_inner();

    if cache.read(&id).await.contains_key(&id) {
        return Ok(HttpResponse::Ok().finish());
    }
    let estate: Option<Estate> = web::block(move || -> Result<Option<Estate>, mysql::Error> {
        let mut conn = db.get().expect("Failed to checkout database connection");
        conn.exec_first("select id,thumbnail,name,description,latitude,longitude,address,rent,door_height,door_width,features from estate where id = ?", (id,))
    })
    .await
    .map_err(|e| {
        log::error!("post_estate_request_document: DB execution error : {:?}", e);
        e
    })?
    .map_err(|e| {
        log::error!("post_estate_request_document: DB execution error : {:?}", e);
        InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
    })?;

    if let Some(estate) = estate {
        cache.write(&id).await.insert(id, estate);
        Ok(HttpResponse::Ok().finish())
    } else {
        Ok(HttpResponse::NotFound().finish())
    }
}
