use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct RangeCondition {
    prefix: String,
    suffix: String,
    pub ranges: Vec<Range>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Range {
    id: i64,
    pub min: i64,
    pub max: i64,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ListCondition {
    list: Vec<String>,
}

pub fn get_range<'a>(cond: &'a RangeCondition, range_id: &str) -> Option<&'a Range> {
    range_id.parse().ok().and_then(|range_index| {
        if range_index < 0 || cond.ranges.len() as i64 <= range_index {
            None
        } else {
            Some(&cond.ranges[range_index as usize])
        }
    })
}
