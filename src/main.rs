use std::env;
use std::fs::File;

use actix_web::{middleware, web, App, HttpServer};
use listenfd::ListenFd;
use tokio::sync::RwLock;

use isuumo::{chair::*, estate::*, initialize::*, map::LockedHashMap};

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    if env::var("RUST_LOG").is_err() {
        env::set_var("RUST_LOG", "actix_server=off,actix_web=off,isuumo=off");
    }
    env_logger::init();

    let mysql_connection_env = web::Data::new(MySQLConnectionEnv::default());
    let chair_search_condition: web::Data<ChairSearchCondition> = web::Data::new({
        let file = File::open("../fixture/chair_condition.json")?;
        serde_json::from_reader(file)?
    });
    let estate_search_condition: web::Data<EstateSearchCondition> = web::Data::new({
        let file = File::open("../fixture/estate_condition.json")?;
        serde_json::from_reader(file)?
    });

    let manager = r2d2_mysql::MysqlConnectionManager::new(
        mysql::OptsBuilder::new()
            .ip_or_hostname(Some(&mysql_connection_env.host))
            .tcp_port(mysql_connection_env.port)
            .user(Some(&mysql_connection_env.user))
            .db_name(Some(&mysql_connection_env.db_name))
            .pass(Some(&mysql_connection_env.password)),
    );
    let pool = web::Data::new(
        r2d2::Pool::builder()
            .max_size(10)
            .build(manager)
            .expect("Failed to create connection pool"),
    );

    let estate_search_cache: web::Data<EstateSearchCache> = web::Data::new(LockedHashMap::new());
    let estate_detail_cache: web::Data<EstateDetailCache> = web::Data::new(LockedHashMap::new());
    let estate_low_priced: web::Data<EstateLowPricedCache> = web::Data::new(RwLock::new(None));
    let chair_detail_cache = web::Data::new(ChairDetailCache::new());
    let chair_low_priced: web::Data<ChairLowPricedCache> = web::Data::new(RwLock::new((None, 0)));

    let mut listenfd = ListenFd::from_env();
    let server = HttpServer::new(move || {
        App::new()
            .app_data(pool.clone())
            .app_data(mysql_connection_env.clone())
            .app_data(chair_search_condition.clone())
            .app_data(estate_search_condition.clone())
            .app_data(estate_search_cache.clone())
            .app_data(estate_detail_cache.clone())
            .app_data(estate_low_priced.clone())
            .app_data(chair_detail_cache.clone())
            .app_data(chair_low_priced.clone())
            .wrap(middleware::Logger::default())
            .route("/initialize", web::post().to(initialize))
            .service(
                web::scope("/api")
                    .service(
                        web::scope("/chair")
                            .route("/search", web::get().to(search_chairs))
                            .route("/low_priced", web::get().to(get_low_priced_chair))
                            .route(
                                "/search/condition",
                                web::get().to(get_chair_search_condition),
                            )
                            .route("/buy/{id}", web::post().to(buy_chair))
                            .route("/{id}", web::get().to(get_chair_detail))
                            .route("", web::post().to(post_chair)),
                    )
                    .service(
                        web::scope("/estate")
                            .route("/search", web::get().to(search_estates))
                            .route("/low_priced", web::get().to(get_low_priced_estate))
                            .route(
                                "/req_doc/{id}",
                                web::post().to(post_estate_request_document),
                            )
                            .route("/nazotte", web::post().to(search_estate_nazotte))
                            .route(
                                "/search/condition",
                                web::get().to(get_estate_search_condition),
                            )
                            .route("/{id}", web::get().to(get_estate_detail))
                            .route("", web::post().to(post_estate)),
                    )
                    .route(
                        "/recommended_estate/{id}",
                        web::get().to(search_recommended_estate_with_chair),
                    ),
            )
    });
    let server = if let Some(l) = listenfd.take_tcp_listener(0)? {
        server.listen(l)?
    } else {
        server.bind((
            "0.0.0.0",
            std::env::var("SERVER_PORT")
                .map(|port_str| port_str.parse().expect("Failed to parse SERVER_PORT"))
                .unwrap_or(1323),
        ))?
    };
    server.run().await
}
