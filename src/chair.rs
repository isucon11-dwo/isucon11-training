use actix_multipart::Multipart;
use actix_web::{error::InternalError, http::StatusCode, web, Error as AWError, HttpResponse};
use bytes::BytesMut;
use futures::TryStreamExt;
use mysql::prelude::*;
use serde::{Deserialize, Serialize};
use tokio::sync::RwLock;

use crate::{common::*, BlockingDBError, Pool, LIMIT};

pub type ChairLowPricedCache = RwLock<(Option<ChairListResponse>, i64)>;

pub struct ChairDetailCache(Vec<RwLock<Option<Chair>>>);

impl ChairDetailCache {
    pub fn new() -> Self {
        let mut v = Vec::with_capacity(40_000);
        for _ in 0..v.capacity() {
            v.push(RwLock::new(None));
        }
        Self(v)
    }

    fn get(&self, id: i64) -> &RwLock<Option<Chair>> {
        &self.0[id as usize]
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct ChairSearchCondition {
    width: RangeCondition,
    height: RangeCondition,
    depth: RangeCondition,
    price: RangeCondition,
    color: ListCondition,
    feature: ListCondition,
    kind: ListCondition,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Chair {
    id: i64,
    name: String,
    description: String,
    thumbnail: String,
    price: i64,
    pub height: i64,
    pub width: i64,
    pub depth: i64,
    color: String,
    features: String,
    kind: String,
    #[serde(skip)]
    stock: i64,
}

impl FromRow for Chair {
    fn from_row_opt(row: mysql::Row) -> Result<Self, mysql::FromRowError> {
        fn convert(row: &mysql::Row) -> Result<Chair, ()> {
            Ok(Chair {
                id: row.get("id").ok_or(())?,
                name: row.get("name").ok_or(())?,
                description: row.get("description").ok_or(())?,
                thumbnail: row.get("thumbnail").ok_or(())?,
                price: row.get("price").ok_or(())?,
                height: row.get("height").ok_or(())?,
                width: row.get("width").ok_or(())?,
                depth: row.get("depth").ok_or(())?,
                color: row.get("color").ok_or(())?,
                features: row.get("features").ok_or(())?,
                kind: row.get("kind").ok_or(())?,
                stock: row.get("stock").ok_or(())?,
            })
        }
        convert(&row).map_err(|_| mysql::FromRowError(row))
    }
}

pub async fn get_chair_detail(
    db: web::Data<Pool>,
    cache: web::Data<ChairDetailCache>,
    path: web::Path<i64>,
) -> Result<HttpResponse, AWError> {
    let id = path.into_inner();

    fn chair_response(chair: &Chair) -> Result<HttpResponse, AWError> {
        if chair.stock <= 0 {
            Ok(HttpResponse::NotFound().finish())
        } else {
            Ok(HttpResponse::Ok().json(chair))
        }
    }

    if let Some(chair) = cache.get(id).read().await.as_ref() {
        return chair_response(chair);
    }

    let chair: Option<Chair> = web::block(move || {
        let mut conn = db.get().expect("Failed to checkout database connection");
        conn.exec_first("select id,name,description,thumbnail,price,height,width,depth,color,features,kind,stock from chair where id = ?", (id,))
    })
    .await
    .map_err(|e| {
        log::error!("Failed to get the chair from id : {:?}", e);
        InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
    })?
    .map_err(|e| {
        log::error!("Failed to get the chair from id : {}", e);
        InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
    })?;

    if let Some(chair) = chair {
        let r = chair_response(&chair);
        cache.get(id).write().await.replace(chair);
        r
    } else {
        log::info!("requested id's chair not found : {}", id);
        Ok(HttpResponse::NotFound().finish())
    }
}

#[derive(Debug, Deserialize, Clone)]
pub struct CSVChair {
    id: i64,
    name: String,
    description: String,
    thumbnail: String,
    price: i64,
    height: i64,
    width: i64,
    depth: i64,
    color: String,
    features: String,
    kind: String,
    popularity: i64,
    stock: i64,
}

impl Into<Chair> for CSVChair {
    fn into(self) -> Chair {
        Chair {
            id: self.id,
            name: self.name,
            description: self.description,
            thumbnail: self.thumbnail,
            price: self.price,
            height: self.height,
            width: self.width,
            depth: self.depth,
            color: self.color,
            features: self.features,
            kind: self.kind,
            stock: self.stock,
        }
    }
}

pub async fn post_chair(
    db: web::Data<Pool>,
    cache: web::Data<ChairDetailCache>,
    cache2: web::Data<ChairLowPricedCache>,
    mut payload: Multipart,
) -> Result<HttpResponse, AWError> {
    let mut chairs: Option<Vec<CSVChair>> = None;
    while let Ok(Some(field)) = payload.try_next().await {
        let content_disposition = field.content_disposition().unwrap();
        let name = content_disposition.get_name().unwrap();
        if name == "chairs" {
            let content = field
                .map_ok(|chunk| BytesMut::from(&chunk[..]))
                .try_concat()
                .await?;
            let mut reader = csv::ReaderBuilder::new()
                .has_headers(false)
                .from_reader(content.as_ref());
            let mut cs = Vec::new();
            for record in reader.deserialize() {
                let chair: CSVChair = record.map_err(|e| {
                    log::error!("failed to read csv: {:?}", e);
                    InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
                })?;
                cs.push(chair);
            }
            chairs = Some(cs);
        }
    }
    if chairs.is_none() {
        log::error!("failed to get from file: no chairs given");
        return Ok(HttpResponse::BadRequest().finish());
    }
    let chairs = chairs.unwrap();
    let chairs2 = chairs.clone();
    web::block(move || -> Result<(), mysql::Error > {
        let mut stmt = String::from("insert into chair (id, name, description, thumbnail, price, height, width, depth, color, features, kind, popularity, stock) values ");
        let mut params = Vec::<mysql::Value>::new();
        for (i, chair) in chairs.into_iter().enumerate() {
            if i != 0 {
                stmt.push(',');
            }
            params.push(chair.id.into());
            params.push(chair.name.into());
            params.push(chair.description.into());
            params.push(chair.thumbnail.into());
            params.push(chair.price.into());
            params.push(chair.height.into());
            params.push(chair.width.into());
            params.push(chair.depth.into());
            params.push(chair.color.into());
            params.push(chair.features.into());
            params.push(chair.kind.into());
            params.push(chair.popularity.into());
            params.push(chair.stock.into());
            stmt.push_str("(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        }
        let mut conn = db.get().expect("Failed to checkout database connection");
        let mut tx = conn.start_transaction(mysql::TxOpts::default())?;
        tx.exec_drop(stmt, params)?;
        tx.commit()?;
        Ok(())
    })
        .await.map_err(|e: BlockingDBError| {
        log::error!("failed to insert/commit chair: {:?}", e);
        e
    })?.map_err(|e| {
        log::error!("failed to read csv: {:?}", e);
        InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
    })?;
    let mut min_price = i64::MAX;
    for chair in chairs2 {
        min_price = min_price.min(chair.price);
        cache.get(chair.id).write().await.replace(chair.into());
    }
    if cache2.read().await.1 > min_price {
        let mut lock = cache2.write().await;
        lock.0.take();
        lock.1 = 0;
    }
    Ok(HttpResponse::Created().finish())
}

#[derive(Debug, Deserialize)]
pub struct SearchChairsParams {
    #[serde(rename = "priceRangeId", default)]
    price_range_id: String,
    #[serde(rename = "heightRangeId", default)]
    height_range_id: String,
    #[serde(rename = "widthRangeId", default)]
    width_range_id: String,
    #[serde(rename = "depthRangeId", default)]
    depth_range_id: String,
    #[serde(default)]
    kind: String,
    #[serde(default)]
    color: String,
    #[serde(default)]
    features: String,
    page: i64,
    #[serde(rename = "perPage")]
    per_page: i64,
}

#[derive(Debug, Serialize)]
pub struct ChairSearchResponse {
    count: i64,
    chairs: Vec<Chair>,
}

pub async fn search_chairs(
    db: web::Data<Pool>,
    query_params: web::Query<SearchChairsParams>,
) -> Result<HttpResponse, AWError> {
    let mut conditions = Vec::new();
    let mut params: Vec<mysql::Value> = Vec::new();

    if !query_params.price_range_id.is_empty() {
        match query_params.price_range_id.parse::<i8>() {
            Ok(i) if 0 <= i && i <= 5 => {
                conditions.push("price_id = ?");
                params.push(i.into());
            }
            _ => return Ok(HttpResponse::BadRequest().finish()),
        }
    }

    if !query_params.height_range_id.is_empty() {
        match query_params.height_range_id.parse::<i8>() {
            Ok(i) if 0 <= i && i <= 3 => {
                conditions.push("height_id = ?");
                params.push(i.into());
            }
            _ => return Ok(HttpResponse::BadRequest().finish()),
        }
    }

    if !query_params.width_range_id.is_empty() {
        match query_params.width_range_id.parse::<i8>() {
            Ok(i) if 0 <= i && i <= 3 => {
                conditions.push("width_id = ?");
                params.push(i.into());
            }
            _ => return Ok(HttpResponse::BadRequest().finish()),
        }
    }

    if !query_params.depth_range_id.is_empty() {
        match query_params.depth_range_id.parse::<i8>() {
            Ok(i) if 0 <= i && i <= 3 => {
                conditions.push("depth_id = ?");
                params.push(i.into());
            }
            _ => return Ok(HttpResponse::BadRequest().finish()),
        }
    }

    if !query_params.kind.is_empty() {
        conditions.push("kind = ?");
        params.push(query_params.kind.clone().into());
    }

    if !query_params.color.is_empty() {
        conditions.push("color = ?");
        params.push(query_params.color.clone().into());
    }

    if !query_params.features.is_empty() {
        for f in query_params.features.split(',') {
            conditions.push("features like concat('%', ?, '%')");
            params.push(f.into());
        }
    }

    if conditions.is_empty() {
        log::info!("Search condition not found");
        return Ok(HttpResponse::BadRequest().finish());
    }

    conditions.push("has_stock = TRUE");

    let per_page = query_params.per_page;
    let page = query_params.page;

    let search_condition = conditions.join(" and ");
    let res: ChairSearchResponse =
        web::block(move || -> Result<ChairSearchResponse, mysql::Error> {
            let mut conn = db.get().expect("Failed to checkout database connection");
            let row = conn.exec_first(
                format!("select count(*) from chair where {}", search_condition),
                &params,
            )?;
            let count = row.map(|(c,)| c).unwrap_or(0);
            if count == 0 {
                return Ok(ChairSearchResponse{count, chairs: Vec::new()});
            }
            params.push(per_page.into());
            params.push((page * per_page).into());
            let chairs = conn.exec(
                format!(
                    "select id,name,description,thumbnail,price,height,width,depth,color,features,kind,stock from chair where {} order by popularity desc, id asc limit ? offset ?",
                    search_condition
                ),
                &params,
            )?;
            Ok(ChairSearchResponse { count, chairs })
        })
            .await
            .map_err(|e: BlockingDBError| {
                log::error!("searchChairs DB execution error : {:?}", e);
                e
            })?
            .map_err(|e| {
                log::error!("searchChairs DB execution error : {:?}", e);
                InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
            })?;
    Ok(HttpResponse::Ok().json(res))
}

#[derive(Debug, Serialize, Clone)]
pub struct ChairListResponse {
    chairs: Vec<Chair>,
}

pub async fn get_low_priced_chair(
    db: web::Data<Pool>,
    cache: web::Data<ChairLowPricedCache>,
) -> Result<HttpResponse, AWError> {
    if let Some(r) = cache.read().await.0.as_ref() {
        return Ok(HttpResponse::Ok().json(r));
    }
    let chairs = web::block(move || {
        let mut conn = db.get().expect("Failed to checkout database connection");
        conn.exec(
            "select id,name,description,thumbnail,price,height,width,depth,color,features,kind,stock from chair where has_stock = TRUE order by price asc, id asc limit ?",
            (LIMIT,),
        )
    })
    .await
    .map_err(|e| {
        log::error!("get_low_priced_chair DB execution error : {:?}", e);
        e
    })?
    .map_err(|e| {
        log::error!("get_low_priced_chair DB execution error : {:?}", e);
        InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
    })?;
    let r = ChairListResponse { chairs };
    let resp = Ok(HttpResponse::Ok().json(&r));
    let mut lock = cache.write().await;
    lock.1 = r.chairs.last().unwrap().price;
    lock.0.replace(r);
    resp
}

pub async fn get_chair_search_condition(
    chair_search_condition: web::Data<ChairSearchCondition>,
) -> Result<HttpResponse, AWError> {
    Ok(HttpResponse::Ok().json(chair_search_condition.as_ref()))
}

#[derive(Debug, Deserialize)]
pub struct BuyChairRequest {
    email: String,
}

pub async fn buy_chair(
    db: web::Data<Pool>,
    path: web::Path<i64>,
    cache: web::Data<ChairDetailCache>,
    cache2: web::Data<ChairLowPricedCache>,
    _params: web::Json<BuyChairRequest>,
) -> Result<HttpResponse, AWError> {
    let id = path.into_inner();

    // web::block の中で await できない・・・
    let mut lock = cache.get(id).write().await;
    let chair = web::block(move || -> Result<Option<Chair>, mysql::Error> {
        let mut conn = db.get().expect("Failed to checkout database connection");
        let mut tx = conn.start_transaction(mysql::TxOpts::default())?;
        let row: Option<Chair> = tx.exec_first(
            "select id,name,description,thumbnail,price,height,width,depth,color,features,kind,stock from chair where id = ? and has_stock = TRUE for update",
            (id,),
        )?;
        if row.is_some() {
            tx.exec_drop("update chair set stock = stock - 1 where id = ?", (id,))?;
            tx.commit()?;
        }
        Ok(row)
    })
    .await
    .map_err(|e: BlockingDBError| {
        log::error!("buy_chair DB execution error : {:?}", e);
        e
    })?
    .map_err(|e| {
        log::error!("buy_chair DB execution error : {:?}", e);
        InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR)
    })?;

    if let Some(chair) = chair {
        if let Some(c) = lock.as_mut() {
            c.stock -= 1;
            if c.stock <= 0 && c.price < cache2.read().await.1 {
                let mut lock = cache2.write().await;
                lock.0.take();
                lock.1 = 0;
            }
        } else {
            lock.replace(chair);
        }
        Ok(HttpResponse::Ok().finish())
    } else {
        Ok(HttpResponse::NotFound().finish())
    }
}
