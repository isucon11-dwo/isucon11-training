alter table isuumo.estate
    add column geo POINT DEFAULT NULL  SRID 4326
;

update isuumo.estate
set geo = ST_GeomFromText(ST_AsText(POINT(latitude, longitude)), 4326);
-- SPATIAL index must be NOT NULL
alter table isuumo.estate
    modify column geo POINT NOT NULL SRID 4326;
alter table isuumo.estate
    add SPATIAL index i_estate_geo (geo);
