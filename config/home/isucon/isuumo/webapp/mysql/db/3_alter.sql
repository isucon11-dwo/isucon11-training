alter table isuumo.chair
    add column has_stock bool as (stock > 0),
    add key i_has_stock_price (has_stock, price, id),
    add column height_id tinyint as (case
                                         when height < 80 then 0
                                         when height < 110 then 1
                                         when height < 150 then 2
                                         else 3 end),
    add column width_id  tinyint as (case
                                         when width < 80 then 0
                                         when width < 110 then 1
                                         when width < 150 then 2
                                         else 3 end),
    add column depth_id  tinyint as (case
                                         when depth < 80 then 0
                                         when depth < 110 then 1
                                         when depth < 150 then 2
                                         else 3 end),
    add column price_id  tinyint as (case
                                         when price < 3000 then 0
                                         when price < 6000 then 1
                                         when price < 9000 then 2
                                         when price < 12000 then 3
                                         when price < 15000 then 4
                                         else 5 end),
    add key i_height_id_p (height_id, popularity desc, id),
    add key i_width_id_p (width_id, popularity desc, id),
    add key i_depth_id_p (depth_id, popularity desc, id),
    add key i_price_id_p (price_id, popularity desc, id),
    add key i_color_p (color, popularity desc, id)
;

alter table isuumo.estate
    add key i_rent_id (rent, id),
    add column door_width_id  tinyint as (case
                                              when door_width < 80 then 0
                                              when door_width < 110 then 1
                                              when door_width < 150 then 2
                                              else 3 end),
    add column door_height_id tinyint as (case
                                              when door_height < 80 then 0
                                              when door_height < 110 then 1
                                              when door_height < 150 then 2
                                              else 3 end),
    add column rent_id        tinyint as (case
                                              when rent < 50000 then 0
                                              when rent < 100000 then 1
                                              when rent < 150000 then 2
                                              else 3 end),
    add key i_door_width_id (door_width_id, popularity desc, id),
    add key i_door_height_id (door_height_id, popularity desc, id),
    add key i_rent_id_2 (rent_id, popularity desc, id),
    add column door_min int AS (IF(door_height < door_width, door_height, door_width)) not null,
    add column door_max int AS (IF(door_height < door_width, door_width, door_height)) not null,
    add key i_door_min (door_min),
    add key i_door_max (door_max)
;
