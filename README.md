# isucon11-training


## リンク

* [ISUCON11 ポータル](https://portal.isucon.net/)
* [ISUCON11 予選レギュレーション](https://isucon.net/archives/55854734.html)
* [ISUCON11 公式 Discord](https://discord.com/channels/857222988088737803/857222988368707589)
* [やることリスト](https://docs.google.com/spreadsheets/d/1zviVM41uatS-5PI5fGMAqqTrEUM19rof30QdbagsQO8/edit#gid=0)


## 事前設定

```bash
cargo install --force cargo-make
```

## コマンド

```bash
# 各サーバのセットアップ
makers setup
# アプリケーションログの確認
makers logs
```

## API

```
// 初期化
POST /initialize

// 椅子
GET  /api/chair/search            search_chairs
GET  /api/chair/low_priced        get_low_priced_chair
GET  /api/chair/search/condition  get_chair_search_condition
POST /api/chair/buy/{id}          buy_chair
GET  /api/chair/{id}              get_chair_detail
POST /api/chair                   post_chair

// 物件
GET  /api/estate/search           search_estates
GET  /api/estate/low_priced       get_low_priced_estate
POST /api/estate/req_doc/{id}     post_estate_request_document
POST /api/estate/nazotte          search_estate_nazotte
GET  /api/estate/search/condition get_estate_search_condition
GET  /api/estate/{id}             get_estate_detail
POST /api/estate                  post_estate

// レコメンド
GET /api/recommended_estate/{id}  search_recommended_estate_with_chair
```